# VSCode Neovim (asvetliakov.vscode-neovim)

https://github.com/asvetliakov/vscode-neovim

- VSCode Neovimをインストールする
- Neovim version 0.5.0 nightlyをインストールする
  - `scoop bucket add versions`
  - `scoop install neovim-nightly`
  
- vscodeのsetting.jsonにnvim.exeのPATHを設定する
  - `"vscode-neovim.neovimExecutablePaths.win32": "C:\\Users\\****\\scoop\\shims\\nvim.exe`

- init.vimを作成する
  - `C:\Users\****\AppData\Local\nvim\init.vim`

- nvim.exeを起動して「PlugInstall」を実行する
  - `:PlugInstall`

- 参考サイト
  - https://wonwon-eater.com/vscode-nvim/
