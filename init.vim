" vim-plugin manager
call plug#begin(stdpath('data') . '/vscode_plugged')
Plug 'asvetliakov/vim-easymotion'
Plug 'bkad/CamelCaseMotion'
Plug 'haya14busa/vim-edgemotion'
Plug 'junegunn/vim-easy-align'
Plug 'machakann/vim-highlightedyank'
Plug 'machakann/vim-sandwich'
Plug 'markonm/traces.vim'
Plug 'thinca/vim-visualstar'
Plug 'vim-jp/vimdoc-ja'
Plug 'wellle/targets.vim'
Plug 'vim-scripts/ReplaceWithRegister'
Plug 'haya14busa/vim-asterisk'
Plug 'rhysd/clever-f.vim'
Plug 'unblevable/quick-scope' 
Plug 'tpope/vim-commentary'
call plug#end()

" define
let mapleader = "\<space>"
let g:highlightedyank_highlight_duration = 200
" vim-asterisk
map g* <Plug>(asterisk-z*)
map g# <Plug>(asterisk-z#)
map * <Plug>(asterisk-gz*)
let g:asterisk#keeppos = 1
" clever-f
" let g:clever_f_use_migemo = 1
let g:clever_f_chars_match_any_signs = ';'
" quick-scope
" let g:qs_highlight_on_keys = ['f', 'F', 't', 'T']
highlight QuickScopePrimary guifg='#afff5f' gui=underline ctermfg=155 cterm=underline
highlight QuickScopeSecondary guifg='#5fffff' gui=underline ctermfg=81 cterm=underline
" set
set clipboard+=unnamedplus
set ignorecase
set smartcase
set autoindent
" easymotion
let g:EasyMotion_keys = 'arenzwkhotisxmyc'
" vim-commentary
autocmd BufNewFile,BufRead *.dart setlocal commentstring=//\ %s
" keymap
map <Leader>f <Plug>(easymotion-bd-f)
map <Leader>s <Plug>(easymotion-bd-f2)
map <leader>l <Plug>(easymotion-bd-jk)
map <Leader>k <Plug>(easymotion-k)
map <Leader>j <Plug>(easymotion-j)
map <Leader> w <Plug>CamelCaseMotion_w
map <Leader> b <Plug>CamelCaseMotion_b
map <Leader> e <Plug>CamelCaseMotion_e
map <Leader> ge <Plug>CamelCaseMotion_ge
omap i<Leader>w <Plug>CamelCaseMotion_iw
xmap i<Leader>w <Plug>CamelCaseMotion_iw
omap i<Leader>b <Plug>CamelCaseMotion_ib
xmap i<Leader>b <Plug>CamelCaseMotion_ib
omap i<Leader>e <Plug>CamelCaseMotion_ie
xmap i<Leader>e <Plug>CamelCaseMotion_ie
nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>
nmap <Leader>Q :q!<CR>
nmap <Leader>x :x<CR>
nmap <Leader>p :call VSCodeNotify('workbench.action.quickOpen')<CR>
nmap <Leader>P :call VSCodeNotify('workbench.action.showCommands')<CR>
nmap <Leader>b :call VSCodeNotify('workbench.action.toggleSidebarVisibility')<CR>
nmap <Leader>o :call VSCodeNotify('workbench.action.gotoSymbol')<CR>
nmap <Leader>t :call VSCodeNotify('workbench.action.showAllSymbols')<CR>
nmap <Leader>g :call VSCodeNotify('workbench.view.scm')<CR>
nmap <Leader>> :call VSCodeNotify('workbench.action.moveEditorRightInGroup')<CR>
nmap <Leader><lt> :call VSCodeNotify('workbench.action.moveEditorLeftInGroup')<CR>
nmap <Leader>h :call VSCodeNotify('editor.action.showHover')<CR>
nmap <C-H> gT
nmap <C-L> gt
nmap <C-w><C-o> :Tabonly<CR>
nmap <Leader>n :tabnew<CR>
nmap ga <Plug>(EasyAlign)
nmap <C-o> :call VSCodeNotify('workbench.action.navigateBack')<CR>
nmap <C-i> :call VSCodeNotify('workbench.action.navigateForward')<CR>
nmap J <Plug>(edgemotion-j)
nmap K <Plug>(edgemotion-k)
nmap <Leader>e :call VSCodeNotify('workbench.view.explorer')<CR>
" nnoremap J 5j
" nnoremap K 5k
nnoremap \ :noh<CR>
nnoremap gJ J
nnoremap 0 ^
nnoremap ^ 0
nnoremap Y y$
nnoremap <C-w><C-o> :call VSCodeNotify('workbench.action.closeOtherEditors')<CR>
nnoremap <C-w><C-o> :call VSCodeNotify('workbench.action.closeOtherEditors')<CR>
nnoremap <enter> :call VSCodeNotify('workbench.action.toggleSidebarVisibility')<CR>
vmap <c-j> <Plug>(edgemotion-j)
vmap <c-k> <Plug>(edgemotion-k)
vnoremap 0 ^
vnoremap ^ 0
vnoremap J 5j
vnoremap K 5k
xmap ga <Plug>(EasyAlign)
" ------- FIX: Keep autoindent enabled for all filetypes all the time ----- "
autocmd BufEnter * silent! set autoindent smartindent
" ------- Stop vscode-neovim from loading remainder of file ----- "
if exists('g:vscode') 
   finish
endif